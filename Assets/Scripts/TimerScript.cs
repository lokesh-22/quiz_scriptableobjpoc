﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerScript : MonoBehaviour
{
    [SerializeField] float timeToCompleteQuestion = 30f;
    [SerializeField] float timeToShowCorrectAnswer = 10f;
    public bool isPlayerAnsweringQuestion;
    float timerValue;

    public bool loadNextQuestion;
    public float fillFraction;

    void Update()
    {
        UpdateTiimer();
    }

    void UpdateTiimer()
    {
        timerValue -= Time.deltaTime;

        if (isPlayerAnsweringQuestion)
        {

            if(timerValue > 0)
            {
                fillFraction = timerValue / timeToCompleteQuestion;
            }
            else
            {
                isPlayerAnsweringQuestion = false;
                timerValue = timeToShowCorrectAnswer;
            }

        }
        else
        {

            if (timerValue > 0)
            {
                fillFraction = timerValue /timeToShowCorrectAnswer;
            }
            else
            {
                isPlayerAnsweringQuestion = true;
                timerValue = timeToCompleteQuestion;
                loadNextQuestion = true;
            }

        }
    }

    public void CancelTimer()
    {
        timerValue = 0;
    }
}
